NextSgad::Engine.routes.draw do
  resources :employee_goal_activities
  resources :goal_units
  resources :employee_messages
  resources :messages
  resources :activities
  resources :results
  resources :justifications
  resources :attendances
  resources :functions
  resources :employee_goals
  resources :goals
  resources :assessments
  resources :configurations
  resources :positions
  resources :employees
  resources :departments

  get 'positions/orgchart', 'positions#orgchart'
end

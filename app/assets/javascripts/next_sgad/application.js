// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require_tree .

jQuery(document).ready(function($) {
  var datascource = {
    'name': 'Lao Lao',
    'title': 'general manager',
    'relationship': { 'children_num': 5 },
    'children': [
      { 'title': 'Bo Miao', 'name': '<span role="img" class="QhTRn" style="background-image: url("https://bitbucket.org/account/IltonFloraIngui/avatar/32/");"><img aria-hidden="true" src="/account/IltonFloraIngui/avatar/32/" class="cbFzAg"></span>', 'relationship': { 'children_num': 0, 'parent_num': 1,'sibling_num': 7 }},
      { 'name': 'Su Miao', 'title': 'department manager', 'relationship': { 'children_num': 2, 'parent_num': 1,'sibling_num': 7 },
        'children': [
          { 'name': 'Tie Hua', 'title': 'senior engineer', 'relationship': { 'children_num': 0, 'parent_num': 1,'sibling_num': 1 }},
          { 'name': 'Hei Hei', 'title': 'senior engineer', 'relationship': { 'children_num': 2, 'parent_num': 1,'sibling_num': 1 },
            'children': [
              { 'name': 'Pang Pang', 'title': 'engineer', 'relationship': { 'children_num': 0, 'parent_num': 1,'sibling_num': 1 }},
              { 'name': 'Xiang Xiang', 'title': 'UE engineer', 'relationship': { 'children_num': 0, 'parent_num': 1,'sibling_num': 1 }}
            ]
          }
        ]
      },
      { 'name': 'Yu Wei', 'title': 'department manager', 'relationship': { 'children_num': 0, 'parent_num': 1,'sibling_num': 7 }},
      { 'name': 'Chun Miao', 'title': 'department manager', 'relationship': { 'children_num': 0, 'parent_num': 1,'sibling_num': 7 }},
      { 'name': 'Yu Tie', 'title': 'department manager', 'relationship': { 'children_num': 0, 'parent_num': 1,'sibling_num': 7 }}
    ]
  };
  // {
  //   'nodeTitle': 'name',
  //   'nodeId': 'id',
  //   'toggleSiblingsResp': false,
  //   'depth': 999,
  //   'chartClass': '',
  //   'exportButton': false,
  //   'exportFilename': 'OrgChart',
  //   'exportFileextension': 'png',
  //   'parentNodeSymbol': 'fa-users',
  //   'draggable': false,
  //   'direction': 't2b',
  //   'pan': false,
  //   'zoom': false,
  //   'zoominLimit': 7,
  //   'zoomoutLimit': 0.5
  // };
  // var oc = $('#orgchart').orgchart({
  //   'data' : datascource,
  //   'depth': 2,
  //   'nodeTitle': 'name',
  //   'nodeContent': 'title',
  //   'toggleSiblingsResp': true
  // });

  $('[data-orgchart-type=1]').orgchart({
    'data' : datascource,
    'depth': 2,
    'createNode': function(node, data) {
      orgChartNode1(node, data);
    },
    'toggleSiblingsResp': true
  });






  function orgChartNode1(node, data){
    // let secondMenuIcon = document.createElement('i'),
    // secondMenu = document.createElement('div');

    // secondMenuIcon.setAttribute('class', 'fa fa-info-circle second-menu-icon');
    // secondMenuIcon.addEventListener('click', (event) => {
    //   event.target.nextElementSibling.classList.toggle('hidden');
    // });
    // secondMenu.setAttribute('class', 'second-menu hidden');
    // secondMenu.innerHTML = `<img class="avatar" src="../img/avatar/${data.id}.jpg">`;
    // node.append(secondMenuIcon);
    // node.append(secondMenu);

    node.append(
      '<div class="content">' + 
        '<div class="orgchart-container">' + 
          `<img class="orgchart-avatar" src="/uploads/user/avatar/1/57394.jpg">` +
          '<div class="text">' +
            `<p class="">Ocup. ${data.title}</p>`+
            `<p class="">Dep. ${data.title}</p>`+
            `<p class="">Niv. ${data.title}</p>`+
          '</div>'+
        '</div>'+
      '</div>'
    );

  }
    
});

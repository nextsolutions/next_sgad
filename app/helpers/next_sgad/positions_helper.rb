module NextSgad
  module PositionsHelper
    # var datascource = {
    #   'name': 'Lao Lao',
    #   'title': 'general manager',
    #   'relationship': { 'children_num': 5 },
    #   'children': [
    #     { 'title': 'Bo Miao', 'name': '<span role="img" class="QhTRn" style="background-image: url("https://bitbucket.org/account/IltonFloraIngui/avatar/32/");"><img aria-hidden="true" src="/account/IltonFloraIngui/avatar/32/" class="cbFzAg"></span>', 'relationship': { 'children_num': 0, 'parent_num': 1,'sibling_num': 7 }},
    #     { 'name': 'Su Miao', 'title': 'department manager', 'relationship': { 'children_num': 2, 'parent_num': 1,'sibling_num': 7 },
    #       'children': [
    #         { 'name': 'Tie Hua', 'title': 'senior engineer', 'relationship': { 'children_num': 0, 'parent_num': 1,'sibling_num': 1 }},
    #         { 'name': 'Hei Hei', 'title': 'senior engineer', 'relationship': { 'children_num': 2, 'parent_num': 1,'sibling_num': 1 },
    #           'children': [
    #             { 'name': 'Pang Pang', 'title': 'engineer', 'relationship': { 'children_num': 0, 'parent_num': 1,'sibling_num': 1 }},
    #             { 'name': 'Xiang Xiang', 'title': 'UE engineer', 'relationship': { 'children_num': 0, 'parent_num': 1,'sibling_num': 1 }}
    #           ]
    #         }
    #       ]
    #     },
    #     { 'name': 'Yu Wei', 'title': 'department manager', 'relationship': { 'children_num': 0, 'parent_num': 1,'sibling_num': 7 }},
    #     { 'name': 'Chun Miao', 'title': 'department manager', 'relationship': { 'children_num': 0, 'parent_num': 1,'sibling_num': 7 }},
    #     { 'name': 'Yu Tie', 'title': 'department manager', 'relationship': { 'children_num': 0, 'parent_num': 1,'sibling_num': 7 }}
    #   ]
    # };
    # Return de position data as the data above
    def org_data(position, aux_data, positions)
      data = {}
      child_positions = positions[:position.id] || []
      children = []
      child_positions_size = child_positions.size
      sibling_num = (positions[:position.position_id] || []).size - 1
      sibling_num = 0 if sibling_num < 0
      parent_num = position.position_id.present? ? 1 : 0
      employee = aux_data[[NextSgad::Position.name, position.employee_id]]&.last
      department = aux_data[[NextSgad::Department.name, position.department_id]]&.last
      data[:id] = postion.id
      data[:name] = employee&.name || 'N/A'
      data[:avatar] = employee&.avatar if employee&.avatar.present?
      data[:ocupation] = position.name
      data[:department] = department&.name if department.present?
      data[:level] = employee&.level if employee.present?
      data[:paygrade] = employee&.paygrade if employee.present?
      data[:department_id] = department&.id if department.present?
      data[:employee_id] = employee&.id if employee.present?
      data[:employee_number] = employee&.number if employee.present?

      child_positions.each do |child|
        children << org_data(child, aux_data, positions)
      end
      data[:children] = children if child_positions_size > 0
      data[:relationship] = {children_num: child_positions_size, sibling_num: sibling_num, parent_num: parent_num}
      data
    end
  end
end

module NextSgad
  class Employee < NextSgad::ApplicationRecord
    # belongs_to :position
  #   has_many :employee_goals, dependent: :destroy
  #   has_many :employee_messages, dependent: :destroy
  #   has_many :attendances, dependent: :destroy
  #   has_and_belongs_to_many :positions, association_foreign_key: :next_sgad_position_id, foreign_key: :next_sgad_employee_id
  #   has_one :efective_position, class_name: NextSgad::Position.name, foreign_key: :efective_id
  #   has_many :employees_assessments, dependent: :destroy
  #   has_many :employee_paygrades
  #   has_many :employee_work_periods
  #   has_many :employee_absences
  #   has_many :employee_delays
  #   has_many :employee_exits
  #   has_many :employee_justifications
  #   has_many :employee_regimes
  #   has_many :approvers
  #   has_many :salaries, through: :employee_paygrades
  #   has_one :latest_paygrade, ->{where(until: nil).order(since: :asc)}, class_name: ::EmployeePaygrade.name, foreign_key: :employee_id

  #   scope :department_id, -> (data) {data = [data].flatten.compact.uniq; data.blank? || data.include?('all') ? all : where(id: NextSgad::Position.where(department_id: data).map(&:efective_id))}
  #   scope :function_id, -> (data) {data = [data].flatten.compact.uniq; data.blank? || data.include?('all') ? all : where(id: NextSgad::Position.where(function_id: data).map(&:efective_id))}
  #   scope :paygrade, -> (data) {data = [data].flatten.compact.uniq; data.blank? || data.include?('all') ? all : where(paygrade: data)}
  #   scope :position_id, -> (data) {data = [data].flatten.compact.uniq; data.blank? || data.include?('all') ? all : where(id: NextSgad::Position.where(id: data).map(&:efective_id))}
  #   scope :level, -> (data) {data = [data].flatten.compact.uniq; data.blank? || data.include?('all') ? all : where(level: data)}

  #   def full_name
  #     "#{first_name} #{middle_name} #{last_name}"
  #   end

  #   def first_and_last_name
  #     "#{first_name} #{last_name}"
  #   end

  #   def full_abbr_name
  #     "#{first_name} #{middle_name.to_s.split(' ').map{|name| "#{name.first.upcase}." if name.present?}.compact.join(' ')} #{last_name}"
  #   end

  #   def name_and_number
  #     "#{number} - #{first_name} #{last_name}"
  #   end

  #   def self.map_for_select
  #     all.map {|f| [f.name_and_number, f.id]}
  #   end

  #   def self.map_for_filter
  #     [[I18n.t(:everything), :all]] + all.map {|f| [f.name_and_number, f.id]}
  #   end

  #   def self.map_paygrade_for_filter
  #     [[I18n.t(:everything), :all]] + all.order(paygrade: :asc).map {|f| [f.paygrade, f.paygrade]}.uniq
  #   end

  #   def self.map_level_for_filter
  #     [[I18n.t(:everything), :all]] + all.order(level: :asc).map {|f| [f.level, f.level]}.uniq
  #   end

  #   def unread_messages
  #     employee_messages.unread
  #   end

  #   def self.can_be_assessed
  #     where(can_be_assessed: true)
  #   end

  #   def self.cannot_be_assessed
  #     where(can_be_assessed: false)
  #   end

  #   def is_assessed?
  #     can_be_assessed
  #   end

  #   def is_not_assessed?
  #     !is_assessed?
  #   end

  #   # creates a new employee_paygrade
  #   def new_paygrade(options = {})
  #     options.merge!({employee_id: id})
  #     EmployeePaygrade.new(options)
  #   end

  #   # creates a new employee_work_period
  #   def new_work_period(options = {})
  #     options.merge!({employee_id: id})
  #     EmployeeWorkPeriod.new(options)
  #   end

  #   # Creates a new goal just for this employee
  #   def new_goal(options = {})
  #     NextSgad::Goal.new(options)
  #   end

  # def has_this_assessment?(assessment_ids)
  #   employees_assessments.select{|e| [assessment_ids].flatten.uniq.compact.include?(e.assessment_id)}.present?
  # end


  #   INITIAL_LETTER = "E"
  #   before_save :create_number, on: :create
  #   after_save :remove_assessment_data_if_employee_not_assessed
  #   validates_presence_of :first_name, :last_name
  #   after_create :create_employees_assessments
  #   validates_uniqueness_of :number, if: -> (employee) {employee.number.present?}

  #   private
  #   # create employee assessment for the current employee
  #   def create_employees_assessments
  #     assessmens = NextSgad::Assessment.active.map{|e| {assessment_id: e.id, employee_id: id}}
  #     # Creates an array like [{employee_id: 1}, {employee_id: 3}]
  #     NextSgad::EmployeeAssessment.create(assessmens)
  #   end
  #   # remove all assessments if the employee can_be_assessed == false
  #   def remove_assessment_data_if_employee_not_assessed
  #     if is_not_assessed?
  #       active_assessment = NextSgad::Assessment.active.last
  #       if active_assessment.present?
  #         employee_goals.where(assessment_id: active_assessment.id).delete_all
  #         attendances.where("extract(year from date) = ?", active_assessment.year).delete_all
  #         employees_assessments.where(assessment_id: active_assessment.id).where(manual: false).delete_all
  #       end
  #     end
  #   end
  end
end

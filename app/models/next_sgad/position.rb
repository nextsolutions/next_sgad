module NextSgad
  class Position < NextSgad::ApplicationRecord
    # belongs_to :department, optional: true
    # belongs_to :position, optional: true
    # belongs_to :function, optional: true
    # belongs_to :efective, optional: true, class_name: NextSgad::Employee.name
    # has_many :positions
    # has_many :attendances
    # # has_many :goals
    # has_many :employee_goals
    # has_and_belongs_to_many :goals, association_foreign_key: :next_sgad_goal_id, foreign_key: :next_sgad_position_id
    # has_and_belongs_to_many :employees, association_foreign_key: :next_sgad_employee_id, foreign_key: :next_sgad_position_id
    # has_and_belongs_to_many :subsidy_types, association_foreign_key: :subsidy_type_id, foreign_key: :next_sgad_position_id
    # has_and_belongs_to_many :tax_types, association_foreign_key: :tax_type_id, foreign_key: :next_sgad_position_id

    # scope :department_id, -> (data) {data = [data].flatten.compact.uniq; data.blank? || data.include?('all') ? all : where(department_id: data)}
    # scope :efective_id, -> (data) {data = [data].flatten.compact.uniq; data.blank? || data.include?('all') ? all : where(efective_id: data)}
    # scope :position_id, -> (data) {data = [data].flatten.compact.uniq; data.blank? || data.include?('all') ? all : where(position_id: data)}

    # def self.ocupied
    #   self.where.not(efective_id: nil)
    # end

    # def name_and_number
    #   "#{number} - #{name}"
    # end

    # # creates filter data
    # def self.map_for_select
    #   all.map {|f| [f.name_and_number, f.id]}
    # end

    # # creates filter data
    # def self.map_for_filter
    #   [[I18n.t(:everything), :all]] + all.map {|f| [f.name_and_number, f.id]}
    # end

    # INITIAL_LETTER = "P"

    # #validates_uniqueness_of :efective_id, if: -> (f) {f.efective_id.present?}, on: :create
    # before_save :create_number, on: :create
    # #before_save :saves_employees
    # #after_create :create_goals_after_create
    # validates_uniqueness_of :number, if: -> (f) {f.number.present?}, on: :create

    # after_create :create_goals_after_create
    # # validates_presence_of :function_id, on: :update
    # # validates_uniqueness_of :number, if: -> (f) {f.number.present?}, on: :create

    # private
    # def saves_employees
    #   return if self.employees_id == nil
    #   self.employee_ids << efective_id
    #   self.employee_ids = self.employee_ids.compact.uniq
    # end

    # # queries the positions belonging to employees tha cannot be assessed
    # def self.can_be_assessed
    #   where(efective_id: [NextSgad::Employee.can_be_assessed.ids, nil])
    # end

    # # queries the positions belonging to employees tha can be assessed
    # def self.cannot_be_assessed
    #   where(efective_id: NextSgad::Employee.cannot_be_assessed.ids)
    # end

    # # create employee goal after creating an position
    # def create_goals_after_create
    #   assessm = NextSgad::Assessment.active.last
    #   return if assessm.nil? || function_id.nil? || efective_id.nil?

    #   employee_goal_data = assessm.goals.map{|g| {status: g.status, goal_type: g.goal_type, name: g.name, unit: g.unit, nature: g.nature, target: g.target, goal_id: g.id, employee_id: efective_id, position_id: id, assessment_id: assessm.id}}
    #   NextSgad::EmployeeGoal.create(employee_goal_data)
    # end

  end
end

module NextSgad
  class Setting < NextSgad::ApplicationRecord
    #
    # This class can't be instantiated... accessing the singleton is done using the 'instance' method
    # such as: NextSgad::Setting.instance
    #
    # in the same matter the 'add' method is done by calling it on the instance
    # such as: NextSgad::Setting.instance.add "is_active", true
    # 
  end
end

require_dependency "next_sgad/application_controller"

module NextSgad
  class EmployeeGoalActivitiesController < ::ApplicationController
    before_action :set_employee_goal_activity, only: [:show, :edit, :update, :destroy]

    # GET /employee_goal_activities
    def index
      @employee_goal_activities = EmployeeGoalActivity.all
    end

    # GET /employee_goal_activities/1
    def show
    end

    # GET /employee_goal_activities/new
    def new
      @employee_goal_activity = EmployeeGoalActivity.new
    end

    # GET /employee_goal_activities/1/edit
    def edit
    end

    # POST /employee_goal_activities
    def create
      @employee_goal_activity = EmployeeGoalActivity.new(employee_goal_activity_params)

      if @employee_goal_activity.save
        redirect_to @employee_goal_activity, notice: 'Employee goal activity was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /employee_goal_activities/1
    def update
      if @employee_goal_activity.update(employee_goal_activity_params)
        redirect_to @employee_goal_activity, notice: 'Employee goal activity was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /employee_goal_activities/1
    def destroy
      @employee_goal_activity.destroy
      redirect_to employee_goal_activities_url, notice: 'Employee goal activity was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_employee_goal_activity
        @employee_goal_activity = EmployeeGoalActivity.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def employee_goal_activity_params
        params.require(:employee_goal_activity).permit(:description, :employee_goal_id, :creator_id, :creator_type, :attachment)
      end
  end
end

require_dependency "next_sgad/application_controller"

module NextSgad
  class ConfigurationsController < ApplicationController
    before_action :set_configuration, only: [:show, :edit, :update, :destroy]

    # GET /configurations
    def index
      @configurations = Configuration.all
    end

    # GET /configurations/1
    def show
    end

    # GET /configurations/new
    def new
      @configuration = Configuration.new
    end

    # GET /configurations/1/edit
    def edit
    end

    # POST /configurations
    def create
      @configuration = Configuration.new(configuration_params)

      if @configuration.save
        redirect_to @configuration, notice: 'Configuration was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /configurations/1
    def update
      if @configuration.update(configuration_params)
        redirect_to @configuration, notice: 'Configuration was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /configurations/1
    def destroy
      @configuration.destroy
      redirect_to configurations_url, notice: 'Configuration was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_configuration
        @configuration = Configuration.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def configuration_params
        params.require(:configuration).permit(:name, :description, :version)
      end
  end
end

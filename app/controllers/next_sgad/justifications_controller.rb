require_dependency "next_sgad/application_controller"

module NextSgad
  class JustificationsController < ::ApplicationController
    before_action :set_justification, only: [:show, :edit, :update, :destroy]

    # GET /justifications
    def index
      @justifications = Justification.all
    end

    # GET /justifications/1
    def show
    end

    # GET /justifications/new
    def new
      @justification = Justification.new
    end

    # GET /justifications/1/edit
    def edit
    end

    # POST /justifications
    def create
      @justification = Justification.new(justification_params)

      if @justification.save
        redirect_to @justification, notice: 'Justification was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /justifications/1
    def update
      if @justification.update(justification_params)
        redirect_to @justification, notice: 'Justification was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /justifications/1
    def destroy
      @justification.destroy
      redirect_to justifications_url, notice: 'Justification was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_justification
        @justification = Justification.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def justification_params
        params.require(:justification).permit(:documents)
      end
  end
end

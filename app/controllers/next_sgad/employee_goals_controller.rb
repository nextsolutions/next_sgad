require_dependency "next_sgad/application_controller"

module NextSgad
  class EmployeeGoalsController < ::ApplicationController
    before_action :set_employee_goal, only: [:show, :edit, :update, :destroy]

    # GET /employee_goals
    def index
      @employee_goals = EmployeeGoal.all
    end

    # GET /employee_goals/1
    def show
    end

    # GET /employee_goals/new
    def new
      @employee_goal = EmployeeGoal.new
    end

    # GET /employee_goals/1/edit
    def edit
    end

    # POST /employee_goals
    def create
      @employee_goal = EmployeeGoal.new(employee_goal_params)

      if @employee_goal.save
        redirect_to @employee_goal, notice: 'Employee goal was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /employee_goals/1
    def update
      if @employee_goal.update(employee_goal_params)
        redirect_to @employee_goal, notice: 'Employee goal was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /employee_goals/1
    def destroy
      @employee_goal.destroy
      redirect_to employee_goals_url, notice: 'Employee goal was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_employee_goal
        @employee_goal = EmployeeGoal.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def employee_goal_params
        params.require(:employee_goal).permit(:self_assessment, :supervisor_assessment, :final_assessment, :status, :percentage)
      end
  end
end

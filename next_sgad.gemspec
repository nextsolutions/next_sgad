$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "next_sgad/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "next_sgad"
  s.version     = NextSgad::VERSION
  s.authors     = ["Sergio Maziano"]
  s.email       = ["sergio@smarttechys.co.ao"]
  s.homepage    = "http://www.smarttechys.co.ao"
  s.summary     = "SGAD"
  s.description = "NEXT SGAD"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_development_dependency "pg"

end

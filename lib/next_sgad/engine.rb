module NextSgad
  class Engine < ::Rails::Engine
    isolate_namespace NextSgad
    require 'jquery-rails'
    require 'font-awesome-rails'
    require 'chartjs-ror'
    # config.to_prepare do
    #   # Make the implementing application's helpers available to the engine.
    #   # This is required for the overriding of engine views and helpers to work correctly.
    #   NextSgad::ApplicationController.helper Rails.application.helpers
    # end
  end
end

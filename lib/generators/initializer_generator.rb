class InitializerGenerator < Rails::Generators::Base
  desc 'This generates a test file for no reason'
  def create_initializer_file
    create_file "config/initializers/initializer.rb", "# Add initialization content here"
  end
end
module NextSgad::Concerns::Models::Employee
  extend ActiveSupport::Concern
 
  # 'included do' causes the included code to be evaluated in the
  # context where it is included (article.rb), rather than being
  # executed in the module's context (blorgh/concerns/models/article).
  included do
    has_many :employee_goals, dependent: :destroy
    has_and_belongs_to_many :positions, association_foreign_key: :next_sgad_position_id, foreign_key: :next_sgad_employee_id
    has_one :efective_position, class_name: NextSgad::Position.name, foreign_key: :efective_id


    INITIAL_LETTER = "E"
    validates_presence_of :first_name, :last_name
    before_save :create_number, on: :create
 
    private
      
  end
 
  def prints
    '"#{title}"'
  end

end
# NextSgad
Short description and motivation.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'next_sgad', path: 'path/to/engine/folder'
```

And then execute:
```bash
$ bundle
```

Configure the Routes:
```bash
mount NextSgad::Engine, at: 'sgad' 
```
Copy the migrations
```bash
$ bin/rails next_sgad:install:migrations
```

Run the migrations
```bash
$ bin/rails db:migrate SCOPE=next_sgad
```

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

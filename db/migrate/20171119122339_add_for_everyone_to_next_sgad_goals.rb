class AddForEveryoneToNextSgadGoals < ActiveRecord::Migration[5.1]
  def change
    add_column :next_sgad_goals, :for_everyone, :boolean, default: false, null: false
  end
end

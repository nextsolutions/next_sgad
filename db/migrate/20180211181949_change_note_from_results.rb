class ChangeNoteFromResults < ActiveRecord::Migration[5.1]
  def change
    change_column :next_sgad_results, :note, :text
  end
end

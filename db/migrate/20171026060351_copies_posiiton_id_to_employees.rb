class CopiesPosiitonIdToEmployees < ActiveRecord::Migration[5.1]
  def change
    data = (NextSgad::Position.all).group_by{|f| [f.class.name, f.employee_id]}
    NextSgad::Employee.all.each do |e|
      e.position_id = data[[NextSgad::Position.name, e.id]]&.last&.id
      e.save
    end
  end
end

class AddPositionToNextSgadEmployeeGoals < ActiveRecord::Migration[5.1]
  def change
    add_reference :next_sgad_employee_goals, :position, foreign_key: {to_table: :next_sgad_positions}
  end
end

class AddEmailToNextSgadEmployee < ActiveRecord::Migration[5.1]
  def change
    add_column :next_sgad_employees, :email, :string
  end
end

class MigrateFillJustificationColumns < ActiveRecord::Migration[5.1]
  def change
    NextSgad::Justification.all.each do |j|
      j.attendances.update_all(justification_status: j.status)
    end
  end
end

class RemoveUnusedColumns < ActiveRecord::Migration[5.1]
  def change
    # from employees
    remove_column :next_sgad_employees, :email, :string
    remove_column :next_sgad_employees, :name, :string
    # from goals
    remove_column :next_sgad_goals, :percentage, :decimal
    remove_column :next_sgad_goals, :percentage_on_the_type, :decimal
    # from employee goals
    remove_column :next_sgad_employee_goals, :percentage, :decimal
    # from justifications
    remove_column :next_sgad_justifications, :assessment_id, :bigint
    remove_column :next_sgad_justifications, :position_id, :bigint
    remove_column :next_sgad_justifications, :department_id, :bigint
    # Just renames this columns
    rename_column :next_sgad_goals, :state, :status
    rename_column :next_sgad_employee_goals, :state, :status
  end
end

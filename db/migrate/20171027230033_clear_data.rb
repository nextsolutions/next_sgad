class ClearData < ActiveRecord::Migration[5.1]
  def change
    NextSgad::EmployeeGoal.all.delete_all
    NextSgad::Goal.all.delete_all
    NextSgad::Position.all.delete_all
  end
end

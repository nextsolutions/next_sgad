class CorrectsStatus < ActiveRecord::Migration[5.1]
  def change

    rename_column :next_sgad_assessments, :state, :status
  end
end

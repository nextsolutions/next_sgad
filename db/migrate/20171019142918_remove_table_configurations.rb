class RemoveTableConfigurations < ActiveRecord::Migration[5.1]
  def change
    drop_table :next_sgad_configurations
  end
end

require 'test_helper'

module NextSgad
  class GoalUnitsControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    setup do
      @goal_unit = next_sgad_goal_units(:one)
    end

    test "should get index" do
      get goal_units_url
      assert_response :success
    end

    test "should get new" do
      get new_goal_unit_url
      assert_response :success
    end

    test "should create goal_unit" do
      assert_difference('GoalUnit.count') do
        post goal_units_url, params: { goal_unit: { plural_name: @goal_unit.plural_name, singular_name: @goal_unit.singular_name, status: @goal_unit.status } }
      end

      assert_redirected_to goal_unit_url(GoalUnit.last)
    end

    test "should show goal_unit" do
      get goal_unit_url(@goal_unit)
      assert_response :success
    end

    test "should get edit" do
      get edit_goal_unit_url(@goal_unit)
      assert_response :success
    end

    test "should update goal_unit" do
      patch goal_unit_url(@goal_unit), params: { goal_unit: { plural_name: @goal_unit.plural_name, singular_name: @goal_unit.singular_name, status: @goal_unit.status } }
      assert_redirected_to goal_unit_url(@goal_unit)
    end

    test "should destroy goal_unit" do
      assert_difference('GoalUnit.count', -1) do
        delete goal_unit_url(@goal_unit)
      end

      assert_redirected_to goal_units_url
    end
  end
end

require 'test_helper'

module NextSgad
  class EmployeeGoalActivitiesControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    setup do
      @employee_goal_activity = next_sgad_employee_goal_activities(:one)
    end

    test "should get index" do
      get employee_goal_activities_url
      assert_response :success
    end

    test "should get new" do
      get new_employee_goal_activity_url
      assert_response :success
    end

    test "should create employee_goal_activity" do
      assert_difference('EmployeeGoalActivity.count') do
        post employee_goal_activities_url, params: { employee_goal_activity: { attachment: @employee_goal_activity.attachment, creator_id: @employee_goal_activity.creator_id, creator_type: @employee_goal_activity.creator_type, description: @employee_goal_activity.description, employee_goal_id: @employee_goal_activity.employee_goal_id } }
      end

      assert_redirected_to employee_goal_activity_url(EmployeeGoalActivity.last)
    end

    test "should show employee_goal_activity" do
      get employee_goal_activity_url(@employee_goal_activity)
      assert_response :success
    end

    test "should get edit" do
      get edit_employee_goal_activity_url(@employee_goal_activity)
      assert_response :success
    end

    test "should update employee_goal_activity" do
      patch employee_goal_activity_url(@employee_goal_activity), params: { employee_goal_activity: { attachment: @employee_goal_activity.attachment, creator_id: @employee_goal_activity.creator_id, creator_type: @employee_goal_activity.creator_type, description: @employee_goal_activity.description, employee_goal_id: @employee_goal_activity.employee_goal_id } }
      assert_redirected_to employee_goal_activity_url(@employee_goal_activity)
    end

    test "should destroy employee_goal_activity" do
      assert_difference('EmployeeGoalActivity.count', -1) do
        delete employee_goal_activity_url(@employee_goal_activity)
      end

      assert_redirected_to employee_goal_activities_url
    end
  end
end

require 'test_helper'

module NextSgad
  class EmployeeGoalsControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    setup do
      @employee_goal = next_sgad_employee_goals(:one)
    end

    test "should get index" do
      get employee_goals_url
      assert_response :success
    end

    test "should get new" do
      get new_employee_goal_url
      assert_response :success
    end

    test "should create employee_goal" do
      assert_difference('EmployeeGoal.count') do
        post employee_goals_url, params: { employee_goal: { final_assessment: @employee_goal.final_assessment, percentage: @employee_goal.percentage, self_assessment: @employee_goal.self_assessment, state: @employee_goal.state, supervisor_assessment: @employee_goal.supervisor_assessment } }
      end

      assert_redirected_to employee_goal_url(EmployeeGoal.last)
    end

    test "should show employee_goal" do
      get employee_goal_url(@employee_goal)
      assert_response :success
    end

    test "should get edit" do
      get edit_employee_goal_url(@employee_goal)
      assert_response :success
    end

    test "should update employee_goal" do
      patch employee_goal_url(@employee_goal), params: { employee_goal: { final_assessment: @employee_goal.final_assessment, percentage: @employee_goal.percentage, self_assessment: @employee_goal.self_assessment, state: @employee_goal.state, supervisor_assessment: @employee_goal.supervisor_assessment } }
      assert_redirected_to employee_goal_url(@employee_goal)
    end

    test "should destroy employee_goal" do
      assert_difference('EmployeeGoal.count', -1) do
        delete employee_goal_url(@employee_goal)
      end

      assert_redirected_to employee_goals_url
    end
  end
end

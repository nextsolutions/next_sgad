require 'test_helper'

module NextSgad
  class GoalsControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    setup do
      @goal = next_sgad_goals(:one)
    end

    test "should get index" do
      get goals_url
      assert_response :success
    end

    test "should get new" do
      get new_goal_url
      assert_response :success
    end

    test "should create goal" do
      assert_difference('Goal.count') do
        post goals_url, params: { goal: { goal_type: @goal.goal_type, name: @goal.name, nature: @goal.nature, percentage: @goal.percentage, percentage_on_the_type: @goal.percentage_on_the_type, references: @goal.references, state: @goal.state, target: @goal.target, unit: @goal.unit } }
      end

      assert_redirected_to goal_url(Goal.last)
    end

    test "should show goal" do
      get goal_url(@goal)
      assert_response :success
    end

    test "should get edit" do
      get edit_goal_url(@goal)
      assert_response :success
    end

    test "should update goal" do
      patch goal_url(@goal), params: { goal: { goal_type: @goal.goal_type, name: @goal.name, nature: @goal.nature, percentage: @goal.percentage, percentage_on_the_type: @goal.percentage_on_the_type, references: @goal.references, state: @goal.state, target: @goal.target, unit: @goal.unit } }
      assert_redirected_to goal_url(@goal)
    end

    test "should destroy goal" do
      assert_difference('Goal.count', -1) do
        delete goal_url(@goal)
      end

      assert_redirected_to goals_url
    end
  end
end

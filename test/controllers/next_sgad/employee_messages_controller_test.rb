require 'test_helper'

module NextSgad
  class EmployeeMessagesControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    setup do
      @employee_message = next_sgad_employee_messages(:one)
    end

    test "should get index" do
      get employee_messages_url
      assert_response :success
    end

    test "should get new" do
      get new_employee_message_url
      assert_response :success
    end

    test "should create employee_message" do
      assert_difference('EmployeeMessage.count') do
        post employee_messages_url, params: { employee_message: { body: @employee_message.body, employee: @employee_message.employee, message: @employee_message.message, signature: @employee_message.signature, status: @employee_message.status, title: @employee_message.title } }
      end

      assert_redirected_to employee_message_url(EmployeeMessage.last)
    end

    test "should show employee_message" do
      get employee_message_url(@employee_message)
      assert_response :success
    end

    test "should get edit" do
      get edit_employee_message_url(@employee_message)
      assert_response :success
    end

    test "should update employee_message" do
      patch employee_message_url(@employee_message), params: { employee_message: { body: @employee_message.body, employee: @employee_message.employee, message: @employee_message.message, signature: @employee_message.signature, status: @employee_message.status, title: @employee_message.title } }
      assert_redirected_to employee_message_url(@employee_message)
    end

    test "should destroy employee_message" do
      assert_difference('EmployeeMessage.count', -1) do
        delete employee_message_url(@employee_message)
      end

      assert_redirected_to employee_messages_url
    end
  end
end

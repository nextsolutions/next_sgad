require 'test_helper'

module NextSgad
  class ResultsControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    setup do
      @result = next_sgad_results(:one)
    end

    test "should get index" do
      get results_url
      assert_response :success
    end

    test "should get new" do
      get new_result_url
      assert_response :success
    end

    test "should create result" do
      assert_difference('Result.count') do
        post results_url, params: { result: { attachment: @result.attachment, note: @result.note, result_type: @result.result_type, state: @result.state } }
      end

      assert_redirected_to result_url(Result.last)
    end

    test "should show result" do
      get result_url(@result)
      assert_response :success
    end

    test "should get edit" do
      get edit_result_url(@result)
      assert_response :success
    end

    test "should update result" do
      patch result_url(@result), params: { result: { attachment: @result.attachment, note: @result.note, result_type: @result.result_type, state: @result.state } }
      assert_redirected_to result_url(@result)
    end

    test "should destroy result" do
      assert_difference('Result.count', -1) do
        delete result_url(@result)
      end

      assert_redirected_to results_url
    end
  end
end

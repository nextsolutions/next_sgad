require 'test_helper'

module NextSgad
  class AttendancesControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    setup do
      @attendance = next_sgad_attendances(:one)
    end

    test "should get index" do
      get attendances_url
      assert_response :success
    end

    test "should get new" do
      get new_attendance_url
      assert_response :success
    end

    test "should create attendance" do
      assert_difference('Attendance.count') do
        post attendances_url, params: { attendance: { date: @attendance.date, employee_note: @attendance.employee_note, status: @attendance.status, supervisor_note: @attendance.supervisor_note } }
      end

      assert_redirected_to attendance_url(Attendance.last)
    end

    test "should show attendance" do
      get attendance_url(@attendance)
      assert_response :success
    end

    test "should get edit" do
      get edit_attendance_url(@attendance)
      assert_response :success
    end

    test "should update attendance" do
      patch attendance_url(@attendance), params: { attendance: { date: @attendance.date, employee_note: @attendance.employee_note, status: @attendance.status, supervisor_note: @attendance.supervisor_note } }
      assert_redirected_to attendance_url(@attendance)
    end

    test "should destroy attendance" do
      assert_difference('Attendance.count', -1) do
        delete attendance_url(@attendance)
      end

      assert_redirected_to attendances_url
    end
  end
end

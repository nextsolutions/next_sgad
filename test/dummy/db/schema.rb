# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180107204831) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "next_sgad_activities", force: :cascade do |t|
    t.string "created_by", default: "", null: false
    t.string "description", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "next_sgad_assessments", force: :cascade do |t|
    t.integer "status", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "year", null: false
    t.decimal "skills_percentage", default: "0.0", null: false
    t.decimal "objectives_percentage", default: "0.0", null: false
    t.decimal "attendance_percentage", default: "0.0", null: false
    t.integer "number_of_four_hours_delay_to_absence", default: 0, null: false
    t.integer "number_of_three_hours_delay_to_absence", default: 0, null: false
    t.integer "number_of_two_hours_delay_to_absence", default: 0, null: false
    t.integer "number_of_one_hour_delay_to_absence", default: 0, null: false
    t.integer "max_absences_for_five", default: 0, null: false
    t.integer "max_absences_for_four", default: 0, null: false
    t.integer "max_absences_for_three", default: 0, null: false
    t.integer "max_absences_for_two", default: 0, null: false
    t.integer "max_absences_for_one", default: 0, null: false
  end

  create_table "next_sgad_attendances", force: :cascade do |t|
    t.datetime "date"
    t.integer "status", null: false
    t.text "employee_note"
    t.text "supervisor_note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "assessment_id"
    t.bigint "position_id"
    t.bigint "employee_id"
    t.bigint "department_id"
    t.integer "justification_status", default: -1, null: false
    t.index ["assessment_id"], name: "index_next_sgad_attendances_on_assessment_id"
    t.index ["department_id"], name: "index_next_sgad_attendances_on_department_id"
    t.index ["employee_id"], name: "index_next_sgad_attendances_on_employee_id"
    t.index ["position_id"], name: "index_next_sgad_attendances_on_position_id"
  end

  create_table "next_sgad_attendances_justifications", id: false, force: :cascade do |t|
    t.bigint "next_sgad_attendance_id", null: false
    t.bigint "next_sgad_justification_id", null: false
    t.index ["next_sgad_attendance_id", "next_sgad_justification_id"], name: "indx_nxt_sgd_tndncs_jstns_on_attendence_id_and_justification_id"
  end

  create_table "next_sgad_departments", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "next_sgad_department_id"
    t.bigint "department_id"
    t.index ["department_id"], name: "index_next_sgad_departments_on_department_id"
    t.index ["next_sgad_department_id"], name: "index_next_sgad_departments_on_next_sgad_department_id"
  end

  create_table "next_sgad_departments_messages", id: false, force: :cascade do |t|
    t.bigint "next_sgad_message_id", null: false
    t.bigint "next_sgad_department_id", null: false
    t.index ["next_sgad_message_id", "next_sgad_department_id"], name: "idx_nt_sd_depents_meges_on_sgad_message_id_and_sgad_depent_id"
  end

  create_table "next_sgad_employee_goals", force: :cascade do |t|
    t.decimal "self_assessment", default: "0.0", null: false
    t.decimal "supervisor_assessment", default: "0.0", null: false
    t.decimal "final_assessment", default: "0.0", null: false
    t.integer "status", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "assessment_id"
    t.bigint "goal_id"
    t.bigint "employee_id"
    t.integer "unit", default: 0, null: false
    t.integer "goal_type", default: 0, null: false
    t.integer "nature", default: 0, null: false
    t.float "target", default: 0.0, null: false
    t.string "name"
    t.text "description"
    t.bigint "position_id"
    t.text "employee_note"
    t.text "supervisor_note"
    t.decimal "amount", default: "0.0", null: false
    t.text "supervisor_supervisor_note"
    t.index ["assessment_id"], name: "index_next_sgad_employee_goals_on_assessment_id"
    t.index ["employee_id"], name: "index_next_sgad_employee_goals_on_employee_id"
    t.index ["goal_id"], name: "index_next_sgad_employee_goals_on_goal_id"
    t.index ["position_id"], name: "index_next_sgad_employee_goals_on_position_id"
  end

  create_table "next_sgad_employee_messages", force: :cascade do |t|
    t.bigint "message_id"
    t.bigint "employee_id"
    t.integer "status", default: 0, null: false
    t.string "title"
    t.text "body"
    t.string "signature"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "read_at"
    t.index ["employee_id"], name: "index_next_sgad_employee_messages_on_employee_id"
    t.index ["message_id"], name: "index_next_sgad_employee_messages_on_message_id"
  end

  create_table "next_sgad_employees", force: :cascade do |t|
    t.string "number"
    t.integer "paygrade"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "level"
    t.string "avatar"
    t.string "first_name"
    t.string "last_name"
    t.string "middle_name"
    t.boolean "can_approve_justifications", default: false, null: false
    t.boolean "can_be_assessed", default: true, null: false
  end

  create_table "next_sgad_employees_assessments", force: :cascade do |t|
    t.integer "self_assessment_status"
    t.integer "supervisor_assessment_status"
    t.integer "final_assessment_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "employee_id"
    t.bigint "assessment_id"
    t.index ["assessment_id"], name: "index_next_sgad_employees_assessments_on_assessment_id"
    t.index ["employee_id"], name: "index_next_sgad_employees_assessments_on_employee_id"
  end

  create_table "next_sgad_employees_messages", id: false, force: :cascade do |t|
    t.bigint "next_sgad_message_id", null: false
    t.bigint "next_sgad_employee_id", null: false
    t.index ["next_sgad_message_id", "next_sgad_employee_id"], name: "index_sgad_eoyees_messages_on_sgad_mage_id_and_sgad_eoyee_id"
  end

  create_table "next_sgad_employees_positions", id: false, force: :cascade do |t|
    t.bigint "next_sgad_employee_id", null: false
    t.bigint "next_sgad_position_id", null: false
    t.index ["next_sgad_employee_id", "next_sgad_position_id"], name: "idx_nxt_sgad_employees_positions_on_employee_id_and_position_id"
  end

  create_table "next_sgad_functions", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
    t.string "number"
  end

  create_table "next_sgad_functions_goals", id: false, force: :cascade do |t|
    t.bigint "next_sgad_goal_id", null: false
    t.bigint "next_sgad_function_id", null: false
    t.index ["next_sgad_goal_id", "next_sgad_function_id"], name: "idx_nxt_sgd_fctns_gls_n_nxt_sgd_goal_id_and_nt_sgd_function_id"
  end

  create_table "next_sgad_functions_messages", id: false, force: :cascade do |t|
    t.bigint "next_sgad_message_id", null: false
    t.bigint "next_sgad_function_id", null: false
    t.index ["next_sgad_message_id", "next_sgad_function_id"], name: "idx_sgad_funions_mesges_on_sgad_mesge_id_and_sgad_funion_id"
  end

  create_table "next_sgad_goals", force: :cascade do |t|
    t.string "name"
    t.integer "goal_type", default: 0, null: false
    t.integer "status", default: 0, null: false
    t.integer "nature", default: 0, null: false
    t.integer "unit", default: 0, null: false
    t.float "target", default: 0.0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "assessment_id"
    t.text "description"
    t.boolean "for_everyone", default: false, null: false
    t.index ["assessment_id"], name: "index_next_sgad_goals_on_assessment_id"
  end

  create_table "next_sgad_goals_positions", id: false, force: :cascade do |t|
    t.bigint "next_sgad_position_id", null: false
    t.bigint "next_sgad_goal_id", null: false
    t.index ["next_sgad_position_id", "next_sgad_goal_id"], name: "index_next_sgad_goals_positions_on_position_id_and_goal_id"
  end

  create_table "next_sgad_justifications", force: :cascade do |t|
    t.json "documents"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "employee_id"
    t.integer "status", default: 0, null: false
    t.text "employee_note"
    t.text "supervisor_note"
    t.integer "first_approval_status", default: 0, null: false
    t.integer "second_approval_status", default: 0, null: false
    t.index ["employee_id"], name: "index_next_sgad_justifications_on_employee_id"
  end

  create_table "next_sgad_messages", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.string "signature"
    t.boolean "send_to_all", default: false, null: false
    t.integer "status", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "next_sgad_messages_positions", id: false, force: :cascade do |t|
    t.bigint "next_sgad_message_id", null: false
    t.bigint "next_sgad_position_id", null: false
    t.index ["next_sgad_message_id", "next_sgad_position_id"], name: "index_sgad_mees_poions_on_sgad_mege_id_and_sgad_poion_id"
  end

  create_table "next_sgad_positions", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "position_id"
    t.bigint "department_id"
    t.bigint "function_id"
    t.bigint "efective_id"
    t.index ["department_id"], name: "index_next_sgad_positions_on_department_id"
    t.index ["efective_id"], name: "index_next_sgad_positions_on_efective_id"
    t.index ["function_id"], name: "index_next_sgad_positions_on_function_id"
    t.index ["position_id"], name: "index_next_sgad_positions_on_position_id"
  end

  create_table "next_sgad_results", force: :cascade do |t|
    t.string "note"
    t.string "attachment"
    t.integer "result_type"
    t.integer "created_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "employee_id"
    t.bigint "assessment_id"
    t.bigint "employee_goal_id"
    t.bigint "goal_id"
    t.integer "state", default: 0, null: false
    t.decimal "result_value", default: "0.0", null: false
    t.integer "result_nature", default: 0, null: false
    t.index ["assessment_id"], name: "index_next_sgad_results_on_assessment_id"
    t.index ["employee_goal_id"], name: "index_next_sgad_results_on_employee_goal_id"
    t.index ["employee_id"], name: "index_next_sgad_results_on_employee_id"
    t.index ["goal_id"], name: "index_next_sgad_results_on_goal_id"
  end

  create_table "next_sgad_settings", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "version"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "next_sgco_companies", force: :cascade do |t|
    t.string "name"
    t.string "number"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "next_sgco_company_admins", force: :cascade do |t|
    t.string "number"
    t.bigint "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_next_sgco_company_admins_on_company_id"
  end

  add_foreign_key "next_sgad_attendances", "next_sgad_assessments", column: "assessment_id"
  add_foreign_key "next_sgad_attendances", "next_sgad_departments", column: "department_id"
  add_foreign_key "next_sgad_attendances", "next_sgad_employees", column: "employee_id"
  add_foreign_key "next_sgad_attendances", "next_sgad_positions", column: "position_id"
  add_foreign_key "next_sgad_departments", "next_sgad_departments"
  add_foreign_key "next_sgad_departments", "next_sgad_departments", column: "department_id"
  add_foreign_key "next_sgad_employee_goals", "next_sgad_assessments", column: "assessment_id"
  add_foreign_key "next_sgad_employee_goals", "next_sgad_employees", column: "employee_id"
  add_foreign_key "next_sgad_employee_goals", "next_sgad_goals", column: "goal_id"
  add_foreign_key "next_sgad_employee_goals", "next_sgad_positions", column: "position_id"
  add_foreign_key "next_sgad_employee_messages", "next_sgad_employees", column: "employee_id"
  add_foreign_key "next_sgad_employee_messages", "next_sgad_messages", column: "message_id"
  add_foreign_key "next_sgad_employees_assessments", "next_sgad_assessments", column: "assessment_id"
  add_foreign_key "next_sgad_employees_assessments", "next_sgad_employees", column: "employee_id"
  add_foreign_key "next_sgad_goals", "next_sgad_assessments", column: "assessment_id"
  add_foreign_key "next_sgad_justifications", "next_sgad_employees", column: "employee_id"
  add_foreign_key "next_sgad_positions", "next_sgad_departments", column: "department_id"
  add_foreign_key "next_sgad_positions", "next_sgad_employees", column: "efective_id"
  add_foreign_key "next_sgad_positions", "next_sgad_functions", column: "function_id"
  add_foreign_key "next_sgad_positions", "next_sgad_positions", column: "position_id"
  add_foreign_key "next_sgad_results", "next_sgad_assessments", column: "assessment_id"
  add_foreign_key "next_sgad_results", "next_sgad_employee_goals", column: "employee_goal_id"
  add_foreign_key "next_sgad_results", "next_sgad_employees", column: "employee_id"
  add_foreign_key "next_sgad_results", "next_sgad_goals", column: "goal_id"
  add_foreign_key "next_sgco_company_admins", "next_sgco_companies", column: "company_id"
end
